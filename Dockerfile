ARG ELIXIR_BASE_IMAGE
FROM ${ELIXIR_BASE_IMAGE}
ARG WKHTMLTOPDF_VERSION
RUN apt-get --allow-releaseinfo-change update \
    && apt-get --assume-yes upgrade \
    && apt-get --assume-yes install rsync openssh-client curl build-essential git imagemagick inotify-tools chromium xvfb \
    && (curl -fsSL https://deb.nodesource.com/setup_lts.x | bash -) \
    && apt-get --assume-yes install nodejs \
    && npm install --global yarn \
    && curl -OL "https://github.com/wkhtmltopdf/packaging/releases/download/$WKHTMLTOPDF_VERSION/wkhtmltox_$WKHTMLTOPDF_VERSION.buster_amd64.deb" \
    && (dpkg -i "wkhtmltox_$WKHTMLTOPDF_VERSION.buster_amd64.deb" || true) \
    && apt-get -f --assume-yes install \
    && dpkg -i "wkhtmltox_$WKHTMLTOPDF_VERSION.buster_amd64.deb" \
    && rm "wkhtmltox_$WKHTMLTOPDF_VERSION.buster_amd64.deb" \
    && apt-get clean
